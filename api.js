'use strict';

const express    = require('express');
const bodyParser = require('body-parser');
const api        = express();
const models     = require('./models');
const async      = require('async');

const ADMIN_TOKEN = '1d100971-0df0-4c16-abce-21474c61439c';

// Returns a list of all companies.
api.get('/company', (req, res) => {
	models.Company.find({}, (err, docs) => {
		if (err) {
			res.statusCode = 500;
			return res.send('There was a problem processing your request.');
		}

		res.json(docs);
	});
});

// Returns a single company with the given id.
api.get('/company/:id', (req, res) => {
	models.Company.findOne({_id: req.params.id}, (err, doc) => {
		if (err) {
			res.statusCode = 500;
			return res.send('There was a problem processing your request');
		}

		if (doc === null) {
			res.statusCode = 404;
			return res.send('Company not found.');
		}

		res.json(doc);
	});
});

// Adds a new company. Admin token required in the header.
api.post('/company', bodyParser.json(), (req, res) => {
	// Check that he's admin.
	if (req.headers['admin_token'] === undefined || req.headers['admin_token'] !== ADMIN_TOKEN) {
		res.statusCode = 401;
		return res.send('You are not admin.');
	}

	// Add new company
	const c = new models.Company(req.body);

	c.save((err, doc) => {
		if (err) {
			if (err.name === 'ValidationError') {
				res.statusCode = 412;
				return res.send('A company requires a name, description and punchcard_lifetime');
			} else {
				res.statusCode = 500;
				return res.send('Error on server');
			}
		}

		res.statusCode = 201;
		res.json(doc);
		return;
	});
});

// Returns a list of all users.
api.get('/user', (req, res) => {
	models.User.find({}, '_id name age gender', (err, docs) => {
		res.json(docs);
	});
});

// Adds a new punchcard for user with given token (in header) and company
// with given id.
api.post('/punchcard/:company_id', bodyParser.json(), (req, res) => {
	let user, company, promise;
	const promises = [];
	// Check if the token header is present.
	if (req.headers.token === undefined) {
		res.statusCode = 401;
		return res.send();
	}

	async.parallel({
		user:    function(cb) {models.User.findOne({token: req.headers.token}, cb);},
		company: function(cb) {models.Company.findOne({_id: req.params.company_id}, cb);}
	}, function(err, result) {
		const company = result.company;
		const user    = result.user;

		// No company found
		if (company === null) {
			res.statusCode = 404;
			return res.send('Company with that id not found.');
		}
		// No user found
		if (user === null) {
			res.statusCode = 401;
			return res.send('User with that token not found.');
		}

		// Check if there is already a punchcard
		models.Punchcard.findOne({company_id: company._id, user_id: user._id})
		.sort({created: -1}).exec((err, doc) => {
			if (err) {
				res.statusCode = 500;
				return res.send();
			}

			// There is already a punchcard between user and company.
			if (doc !== null) {
				// Get difference in days between creation date and now.
				const diff = new Date().getDate() - doc.created.getDate();
				if (diff <= company.punchcard_lifetime) {
					res.statusCode = 409;
					return res.send('You already have a valid punchcard.');
				}
			}

			// Create a new punchcard
			const p = new models.Punchcard({company_id: company._id, user_id: user._id});
			p.save((err, doc) => {
				if (err) {
					res.statusCode = 500;
					return res.send();
				}
				res.statusCode = 201;
				return res.json(doc);
			});
		});
	});
});

module.exports = api;
