'use strict';

const express  = require('express');
const api      = require('./api');
const mongoose = require('mongoose');
const app      = express();

app.use('/api', api);

mongoose.connect('localhost/punchapi');
mongoose.connection.once('open', () => {
	app.listen(process.env.PORT || 4321);
});
