'use strict';

const mongoose = require('mongoose');
const uuid     = require('node-uuid');

const UserSchema = mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	token: {
		type:String,
		default: uuid.v1()
	},
	age: {
		type: Number,
		required: true
	},
	gender: {
		type: String,
		required: true
	}
});

const CompanySchema = mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	punchcard_lifetime: {
		type: Number,
		required: true
	}
});

const PunchcardSchema = mongoose.Schema({
	company_id: {
		type: String,
		required: true
	},
	user_id: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		default: Date.now
	}
});

module.exports = {
	User:      mongoose.model('User', UserSchema),
	Company:   mongoose.model('Company', CompanySchema),
	Punchcard: mongoose.model('Punchcard', PunchcardSchema)
};
